/**
 * Pick items in array.
 * Use description object to describe items structure and set items selectors.
 */
class ItemsPicker {

	/**
	 * Create class from item description object like:
	 * {
	 *    rootItemTag: "#projects-html",
	 *    itemTag: "tr",
	 *    item: {
			title: "td.left a.bigger",
			category: "td.left>div>small",
			price: "td.text-center span",
			dataPublished: ($item) => $item.getAttribute("data-published")
	 *    }
	 *  }
	 * @param itemDescription - item description object.
	 */
	constructor(itemDescription) {
		this.itemDescription = itemDescription;
	}

	/**
	 * Extract items to array.
	 * Array contains item objects with fields described in description object (constructor parameter)
	 * and values - the text of according HTML elements.
	 * @returns {Array} array of items objects
	 */
	getItems() {
		let resultItemsArray = [];
		let itemFieldNameArray = Object.keys(this.itemDescription.item);

		this.applyEach(itemElement => {

			let resultItemObject = {};

			for (let itemFieldName of itemFieldNameArray) {

				let itemFieldSelector = this.itemDescription.item[itemFieldName];
				let itemFieldValue = null;

				switch (typeof (itemFieldSelector)) {
					case "string":
						itemFieldValue = itemElement.querySelector(itemFieldSelector).textContent.trim();
						break;
					case "function":
						itemFieldValue = itemFieldSelector(itemElement);
						break;
				}

				resultItemObject[itemFieldName] = (!!itemFieldValue) ? itemFieldValue : null;
			}

			resultItemsArray.push(resultItemObject);
		});

		return resultItemsArray;
	}

	/**
	 * Find each item and apply func for it.
	 * Search depends from fields in description object (constructor parameter)
	 * @param func - function to apply on each item
	 */
	applyEach(func) {
		// Find root element for all items
		let rootItemElement = document.querySelector(this.itemDescription.rootItemTag);

		// Find each item element in root
		let itemElements = rootItemElement.querySelectorAll(this.itemDescription.itemTag);

		// Call func for each item element
		[].slice.call(itemElements).forEach(itemElement => func(itemElement));
	}
}